//
//  Model.cpp
//  zeptoTest
//
//  Created by Ivan Lapshin on 27.02.16.
//  Copyright © 2016 Ivan Lapshin. All rights reserved.
//

#include "Model.h"
#include "Bird.h"
#include "Wall.h"
#include <math.h>
#include <stdlib.h>

Model::Model()
{
    time = 0.0;
    dt = 0.1;
    bottomY = -55;
    topY = 55;
    gravity = 9.8;
    minBirdSpeed = -15;
    maxBirdSpeed = 25;
    wallsCount = 8;
    wallsWidth = 12;
    wallsStep = 30;
    wallsSpeed = 0.35;
    minWallHeight = 15;
    maxWallHeight = 60;
    wallStartX = 120;
    wallStopX = wallStartX - wallsCount * (wallsStep + wallsWidth);
        
    bird = new Bird();
    allObjects.push_back(bird);
    
    floor = new Wall(3200, 10);
    floor->shaderName = "Floor";
    floor->setColor(0x0F, 0x30, 0x57, 0xFF);//#0F3057
    floor->posY = bottomY - 5;
    floor->posZ = -40;
    allObjects.push_back(floor);
    
    
    roof = new Wall(3200, 10);
    roof->setColor(0x0F, 0x30, 0x57, 0xFF);//#0F3057
    roof->posY = topY + 5;
    roof->posZ = -40;
    allObjects.push_back(roof);
        
    for(int i = 0; i < wallsCount; i++)
    {
        Wall* top_wall = new Wall();
        Wall* bottom_wall = new Wall();
        top_wall->setColor(0x00, 0x58, 0x7A, 0xFF);//#0F3057
        bottom_wall->setColor(0x00, 0x58, 0x7A, 0xFF);//#0F3057
        
        allObjects.push_back(top_wall);
        allObjects.push_back(bottom_wall);
        walls.push_back(top_wall);
        walls.push_back(bottom_wall);
    }
    restart();
}

Model::~Model()
{
    for (int i = 0; i < allObjects.size(); i++)
    {
        delete allObjects.at(i);
    }
}

bool Model::checkCollisions()
{
    //check if bird touches floor or roof
    if (bird->posY - bird->radius < bottomY)
    {
        bird->posY = bottomY + bird->radius;
        return true;
    }
    else if (bird->posY + bird->radius > topY)
    {
        bird->posY = topY - bird->radius;
        return true;
    }
    float possible_min_x = - bird->radius + bird->posX;//but pos_x is always zero
    float possible_max_x = bird->radius + bird->posX;
    for (int i = 0; i < walls.size(); i++)
    {
        //check if wall is too far on x axis
        if (walls.at(i)->posX + walls.at(i)->wallWidth / 2 < possible_min_x)
            continue;
        if (walls.at(i)->posX - walls.at(i)->wallWidth / 2 > possible_max_x)
            continue;
        
        float distance_y = fabsf(walls.at(i)->posY - bird->posY);
        distance_y = distance_y - walls.at(i)->wallHeight / 2;
        
        if (distance_y > bird->radius)
            continue;
        if (distance_y < 0)
            return true;
        float sina = distance_y / bird->radius; // y = r * sin(a)
        float cosa = sqrtf(1 - sina * sina); // cos^2 + sin^2 = 1
        float new_possible_max_x = cosa * bird->radius;
        float new_possible_min_x = - new_possible_max_x;
        
        if (walls.at(i)->posX + walls.at(i)->wallWidth / 2 < new_possible_min_x)
            continue;
        if (walls.at(i)->posX - walls.at(i)->wallWidth / 2 > new_possible_max_x)
            continue;
        return true;
    }
    return false;
}

void Model::generateWallSize(Wall* top_wall, Wall* bottom_wall)
{
    if (top_wall == NULL || bottom_wall == NULL)
        return;
    int random_top_height = rand() % (maxWallHeight - minWallHeight) + minWallHeight;
    int random_bottom_height = minWallHeight + maxWallHeight - random_top_height;
    
    top_wall->setRectangle(wallsWidth, random_top_height);
    bottom_wall->setRectangle(wallsWidth, random_bottom_height);
    
    top_wall->posY = topY - random_top_height / 2;
    bottom_wall->posY = bottomY + random_bottom_height / 2;
}

void Model::update()
{
    time += dt;
    if (playing == false)
        return;
    
    float dv = - gravity * dt;
    //update speed
    if (bird->speed >= maxBirdSpeed)
        bird->speed = maxBirdSpeed;
    if (bird->speed <= minBirdSpeed)
        bird->speed = minBirdSpeed;
    else
        bird->posY += dv / 2;
    bird->posY += bird->speed * dt;
    bird->speed += dv;
    //update walls
    for(int i = 0; i < walls.size(); i += 2)
    {
        walls.at(i)->posX -= wallsSpeed;
        walls.at(i + 1)->posX -= wallsSpeed;
        
        if ((walls.at(i)->posX < (bird->posX - bird->radius)) && walls.at(i)->passed == false)
        {
            walls.at(i)->passed = true;
            score++;
        }
        
        //walls hiding
        if (walls.at(i)->passed == true)
        {
            if (walls.at(i)->wallHeight != 0)
            {
                walls.at(i)->posY = topY - walls.at(i)->wallHeight / 2;
                walls.at(i)->wallHeight--;
                
            }
            if (walls.at(i + 1)->wallHeight != 0)
            {
                walls.at(i + 1)->posY = bottomY + walls.at(i + 1)->wallHeight / 2;
                walls.at(i + 1)->wallHeight--;
                
            }
        }
        //new walls
        if ((walls.at(i)->posX <= wallStopX) && (walls.at(i + 1)->posX <= wallStopX))
        {
            walls.at(i)->posX = wallStartX;
            walls.at(i + 1)->posX = wallStartX;
            generateWallSize(walls.at(i), walls.at(i + 1));
        }
    }
    
    gameOver = checkCollisions();
    if (gameOver)
        playing = false;
}

std::vector<ObjectGL*>* Model::getObjects()
{
     return &allObjects;
}

bool Model::getPlaying()
{
    return playing;
}

void Model::tap()
{
    if (gameOver)
    {
        restart();
        return;
    }
    if (playing == false)
        playing = true;
    bird->speed += 40;
}

void Model::restart()
{
    playing = false;
    gameOver = false;
    score = 0;
    
    bird->posY = 0;
    bird->speed = 0;
    for(int i = 0; i < walls.size(); i += 2)
    {
        generateWallSize(walls.at(i), walls.at(i + 1));
        walls.at(i)->posX = wallStartX + (wallsStep + wallsWidth) * (i / 2);
        walls.at(i + 1)->posX = wallStartX + (wallsStep + wallsWidth) * (i / 2);
    }

}