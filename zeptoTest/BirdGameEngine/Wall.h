//
//  Wall.h
//  zeptoTest
//
//  Created by Ivan Lapshin on 27.02.16.
//  Copyright © 2016 Ivan Lapshin. All rights reserved.
//

#ifndef Wall_h
#define Wall_h

#include <stdio.h>
#include "ObjectGL.h"

class Wall : public ObjectGL
{
public:
    Wall();
    Wall(float w, float h);
    ~Wall();
    float wallWidth;
    float wallHeight;
    bool passed;

    void setColor(float r, float g, float b, float a);
    void setRectangle(float w, float h);
    virtual const float* getModelView();
};

#endif /* Wall_h */
