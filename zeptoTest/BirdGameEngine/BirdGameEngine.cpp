//
//  BirdGameEngine.cpp
//  zeptoTest
//
//  Created by Ivan Lapshin on 25.02.16.
//  Copyright © 2016 Ivan Lapshin. All rights reserved.
//

#include "BirdGameEngine.h"

#ifdef WIN
    #define BGE_SLASH "\\"
#else
    #define BGE_SLASH "/"
#endif

#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>
#include <math.h>
#include <string>
#include <vector>
#include <map>
#include "Model.h"
#include "ObjectGL.h"

struct ShaderGL
{
    GLuint positionAttrib;
    GLuint colorAttrib;
    GLuint projectionUniform;
    GLuint modelViewUniform;
    GLuint timeUniform;
    GLuint resolutionUniform;
    GLuint programName;
};

typedef std::map<const std::string, ShaderGL*> ShaderMap;

class BirdGameEngine
{
    //EAGLContext*    context;
    //CAEAGLLayer*    eaglLayer;
public:
    BirdGameEngine(std::string _shadersPath);
    ~BirdGameEngine();
    void setSize(int w, int h);
    void handleTap();
    void render();
private:    
    GLint width;
    GLint height;
    
    ShaderMap shaders;
    std::string shadersPath;
    float projection[16];
    float time;
    Model* world;
    
    void setupBuffers();
    void setupShaders();
    
    GLuint compileShader(std::string name, GLenum type);
    void makePerspective(float fov, float aspect, float near, float far);
};
//////////////////////////////////////////////
BGE_DECL_EXPORT void* InitGame(const char *shadersPath, const int size)
{
    std::string path(shadersPath, size);
    return new BirdGameEngine(path);
}

void BGE_EXPORT SetSize(const void* handle, const int w, const int h)
{
    if (handle) ((BirdGameEngine*)handle)->setSize(w, h);
}

void BGE_EXPORT Tap(const void* handle)
{
    if (handle) ((BirdGameEngine*)handle)->handleTap();
}

void BGE_EXPORT Render(const void* handle)
{
    if (handle) ((BirdGameEngine*)handle)->render();
}

void BGE_EXPORT DeleteGame(const void* handle)
{
    if (handle) delete ((BirdGameEngine*)handle);
}
//////////////////////////////////////////////
BirdGameEngine::BirdGameEngine(std::string _shadersPath)
{
    world = new Model();
    time = 0.0;
    shadersPath = _shadersPath;
    shaders.clear();
    
    setupBuffers();
    setupShaders();
    
    glClearColor(0xE7/255.0, 0xE7/255.0, 0xDE/255.0, 1.0);
    glClearDepthf(1.0);
    glDepthFunc(GL_LEQUAL);
    glDepthMask(true);
    glEnable(GL_DEPTH_TEST);
}

BirdGameEngine::~BirdGameEngine()
{
    for (ShaderMap::iterator it = shaders.begin(); it != shaders.end(); ++it)
    {
        if (it->second != NULL)
            delete it->second;
    }
    shaders.clear();
    delete world;
}

void BirdGameEngine::setSize(int w, int h)
{
    width = w;
    height = h;
    float aspect = (float)width / (float)height;
    makePerspective(80, aspect, 0.1, 100.0);
}

void BirdGameEngine::setupBuffers()
{
    std::vector<ObjectGL*>* objects = world->getObjects();
    
    for (int i = 0; i < objects->size(); i++)
    {
        ObjectGL* obj = objects->at(i);
        GLuint buffer;
        glGenBuffers(1, &buffer);
        obj->vertex3BufferName = buffer;
        glBindBuffer(GL_ARRAY_BUFFER, obj->vertex3BufferName);
        glBufferData(GL_ARRAY_BUFFER, obj->vertices3Count * sizeof(float) * 3, obj->vertices3, GL_STATIC_DRAW);
        
        glGenBuffers(1, &buffer);
        obj->color4BufferName = buffer;
        glBindBuffer(GL_ARRAY_BUFFER, obj->color4BufferName);
        glBufferData(GL_ARRAY_BUFFER, obj->colors4Count * sizeof(float) * 4, obj->colors4, GL_STATIC_DRAW);
    }
}

void BirdGameEngine::setupShaders()
{
    std::vector<ObjectGL*>* objects = world->getObjects();
    ObjectGL* obj;
    ShaderGL* shader;
    ShaderMap::iterator it;
    for (int i = 0; i < objects->size(); i++)
    {
        obj = objects->at(i);
        it = shaders.find(obj->shaderName);
        if (it != shaders.end())
            continue;
        
        //lets go create new shader
        shader = NULL;
        std::string vertexName = obj->shaderName + "Vertex.glsl";
        std::string fragmentName =  obj->shaderName + "Fragment.glsl";
        
        GLuint vertexShader = compileShader(vertexName, GL_VERTEX_SHADER);
        GLuint fragmentShader = compileShader(fragmentName, GL_FRAGMENT_SHADER);
        if (vertexShader == -1 || fragmentShader == -1)
        {
            shaders.emplace(obj->shaderName, shader);
            continue;
        }
        GLuint programHandle = glCreateProgram();
        glAttachShader(programHandle, vertexShader);
        glAttachShader(programHandle, fragmentShader);
        glLinkProgram(programHandle);
        GLint linkSuccess;
        glGetProgramiv(programHandle, GL_LINK_STATUS, &linkSuccess);
        
        if (linkSuccess == GL_FALSE)
        {
            shaders.emplace(obj->shaderName, shader);
            continue;
        }
        
        shader = new ShaderGL();
        glUseProgram(programHandle);
        shader->programName = programHandle;
        shader->positionAttrib = glGetAttribLocation(programHandle, "position");
        shader->colorAttrib = glGetAttribLocation(programHandle, "sourceColor");
        shader->projectionUniform = glGetUniformLocation(programHandle, "projection");
        shader->modelViewUniform = glGetUniformLocation(programHandle, "modelView");
        shader->timeUniform = glGetUniformLocation(programHandle, "time");
        shader->resolutionUniform = glGetUniformLocation(programHandle, "resolution");
        
        shaders.emplace(obj->shaderName, shader);
    }

}

GLuint BirdGameEngine::compileShader(std::string name, GLenum type)
{
    std::string path = shadersPath + BGE_SLASH + name;
    FILE* f = fopen(path.data(), "r");
    if (f == NULL)
        return -1;
    fseek(f , 0 , SEEK_END);
    size_t size = ftell(f);
    rewind(f);
    char* shader = new char[size];
    size_t result = fread(shader, 1, size, f);
    fclose(f);
    if (result != size)
        return -1;

    GLuint handle = glCreateShader(type);
    GLint shaderLength = (GLint)result;
    glShaderSource(handle, 1, &shader, &shaderLength);
    
    delete [] shader;
    
    glCompileShader(handle);
    
    GLint compileSuccess;
    glGetShaderiv(handle, GL_COMPILE_STATUS, &compileSuccess);
    if (compileSuccess == GL_FALSE)
    {
        return -1;
    }
    
    return handle;
}


void BirdGameEngine::render()
{
    world->update();
    if (world->getPlaying() == true)
        time += 0.005275;

    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    std::vector<ObjectGL*>* objects = world->getObjects();

    //todo: use only one vertex buffer for walls
    //todo: print score
    ObjectGL* obj;
    ShaderGL* shader;
    ShaderMap::iterator it;
    for (int i = 0; i < objects->size(); i++)
    {
        obj = objects->at(i);
        it = shaders.find(obj->shaderName);
        if (it == shaders.end())
            continue;
        shader = it->second;
        if (shader == NULL)
            continue;
        glUseProgram(shader->programName);
        glEnableVertexAttribArray(shader->positionAttrib);
        glEnableVertexAttribArray(shader->colorAttrib);

        glBindBuffer(GL_ARRAY_BUFFER, obj->vertex3BufferName);
        glVertexAttribPointer(shader->positionAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);

        glBindBuffer(GL_ARRAY_BUFFER, obj->color4BufferName);
        glVertexAttribPointer(shader->colorAttrib, 4, GL_FLOAT, GL_FALSE, 0, 0);

        glUniformMatrix4fv(shader->projectionUniform, 1, 0, projection);
        glUniformMatrix4fv(shader->modelViewUniform, 1, 0, obj->getModelView());
        glUniform1f(shader->timeUniform, time);
        glUniform2f(shader->resolutionUniform, width, height);
        glDrawArrays(obj->drawType, 0, obj->vertices3Count);
    }
}

void BirdGameEngine::handleTap()
{
    if (world->getPlaying() == false)
        time = 0;
    world->tap();
}

void BirdGameEngine::makePerspective(float fov, float aspect, float near, float far)
{
    float top = near * tan(fov * M_PI / 360.0);
    float bottom = -top;
    float left = bottom * aspect;
    float right = top * aspect;
    
    projection[0] = 2 * near / (right - left);
    projection[1] = 0;
    projection[2] = 0;
    projection[3] = 0;
    projection[4] = 0;
    projection[5] = 2 * near / (top - bottom);
    projection[6] = 0;
    projection[7] = 0;
    projection[8] = (right + left) / (right - left);
    projection[9] = (top + bottom) / (top - bottom);
    projection[10] = - (far + near) / (far - near);
    projection[11] = -1;
    projection[12] = 0;
    projection[13] = 0;
    projection[14] = (-2 * far * near) / (far - near);
    projection[15] = 0;
}











