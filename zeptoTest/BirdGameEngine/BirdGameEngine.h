//
//  BirdGameEngine.h
//  zeptoTest
//
//  Created by Ivan Lapshin on 25.02.16.
//  Copyright © 2016 Ivan Lapshin. All rights reserved.
//

#ifndef BirdGameEngine_h
#define BirdGameEngine_h

#include <stdio.h>

#ifdef WIN
#  define BGE_DECL_EXPORT     __declspec(dllexport)
#  define BGE_DECL_IMPORT     __declspec(dllimport)
#else
#  define BGE_DECL_EXPORT     __attribute__((visibility("default")))
#  define BGE_DECL_IMPORT     __attribute__((visibility("default")))
#  define BGE_DECL_HIDDEN     __attribute__((visibility("hidden")))
#endif

#ifdef BGE_LIB
# define BGE_EXPORT BGE_DECL_EXPORT
#else
# define BGE_EXPORT BGE_DECL_IMPORT
#endif


#if __cplusplus
extern "C" {
#endif
    BGE_EXPORT void* InitGame(const char *shadersPath, const int size);
    void BGE_EXPORT SetSize(const void* handle, const int w, const int h);
    void BGE_EXPORT Tap(const void* handle);
    void BGE_EXPORT Render(const void* handle);
    void BGE_EXPORT DeleteGame(const void* handle);
#if __cplusplus
}
#endif


#endif /* BirdGameEngine_h */
