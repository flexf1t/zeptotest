//
//  Bird.h
//  zeptoTest
//
//  Created by Ivan Lapshin on 25.02.16.
//  Copyright © 2016 Ivan Lapshin. All rights reserved.
//

#ifndef Bird_h
#define Bird_h

#include <stdio.h>

#include "ObjectGL.h"

class Bird : public ObjectGL
{
public:
    Bird();
    ~Bird();
    float radius;
    float speed;
};

#endif /* Bird_h */
