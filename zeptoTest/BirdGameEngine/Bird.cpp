//
//  Bird.cpp
//  zeptoTest
//
//  Created by Ivan Lapshin on 25.02.16.
//  Copyright © 2016 Ivan Lapshin. All rights reserved.
//

#include "Bird.h"
#include <math.h>

Bird::Bird() :
    ObjectGL()
{
    drawType = GL_TRIANGLE_FAN;
    posZ = -35;
    radius = 4.0;
    speed = 0;
    shaderName = "Bird";
    //VEC
    vertices3Count = 100;
    vertices3 = new float[vertices3Count * 3]; // (x,y,z) for each vertex
    //center in 0
    vertices3[0] = posX;
    vertices3[1] = posY;
    vertices3[2] = posZ;
    int j = 0;
    for (int i = 0; i < vertices3Count - 1; i++)
    {
        float percent = (i / (float) (vertices3Count - 1 - 1));
        float rad = percent * 2 * M_PI;
        j += 3;
        vertices3[j] = posX + radius * cos(rad);
        vertices3[j + 1] = posY + radius * sin(rad);
        vertices3[j + 2] = posZ;
    }
    vertices3[j + 1] = 0;//because sin(2pi) != 0
        
    //COL
    colors4Count = vertices3Count;
    colors4 = new float[colors4Count * 4];
    j = 0;
    for (int i = 0; i < colors4Count; i++)
    {
        colors4[j] = 0x00 / 255.0;
        colors4[j + 1] = 0x88 / 255.0;
        colors4[j + 2] = 0x91 / 255.0;
        colors4[j + 3] = 1.0;
        j += 4;
    }
}

Bird::~Bird()
{
    
}