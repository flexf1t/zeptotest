//
//  Model.h
//  zeptoTest
//
//  Created by Ivan Lapshin on 27.02.16.
//  Copyright © 2016 Ivan Lapshin. All rights reserved.
//

#ifndef Model_h
#define Model_h

#include <stdio.h>
#include <vector>

class Bird;
class Wall;
class ObjectGL;

class Model
{
private:
    std::vector<ObjectGL*>  allObjects;
    std::vector<Wall*>      walls;
    
    Bird* bird;
    Wall* floor;
    Wall* roof;

    double time;
    bool playing;
    bool gameOver;
    int score;
    
    int wallsCount;
    int wallsWidth;
    float wallsStep;
    float wallsSpeed;
    int minWallHeight;
    int maxWallHeight;
    int wallStartX;
    int wallStopX;
    
    float topY;
    float bottomY;
    float gravity;
    float minBirdSpeed;
    float maxBirdSpeed;
    
    float dt;
    
    bool checkCollisions();
    void generateWallSize(Wall* top_wall, Wall* bottom_wall);
public:
    Model();
    ~Model();
    void update();
    std::vector<ObjectGL*>* getObjects();
    bool getPlaying();
    void tap();
    void restart();
};

#endif /* Model_h */
