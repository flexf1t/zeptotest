//
//  Wall.cpp
//  zeptoTest
//
//  Created by Ivan Lapshin on 27.02.16.
//  Copyright © 2016 Ivan Lapshin. All rights reserved.
//

#include "Wall.h"

Wall::Wall() :
    ObjectGL()
{
    posZ = -35;
    shaderName = "Wall";
    passed = false;
    //basic 1x1 box
    vertices3Count = 4;
    vertices3 = new float[vertices3Count * 3]; // (x,y,z) for each vertex
    //float min_x = rectangle.origin.x - width / 2;
    //float min_y = rectangle.origin.y - height / 2;
    //float max_x = rectangle.origin.x + width / 2;
    //float max_y = rectangle.origin.y + height / 2;
    vertices3[0] = 1;//max_x;
    vertices3[1] = 1;//max_y;
    vertices3[2] = posZ;
    vertices3[3] = -1;//min_x;
    vertices3[4] = 1;//max_y;
    vertices3[5] = posZ;
    vertices3[6] = 1;//max_x;
    vertices3[7] = -1;//min_y;
    vertices3[8] = posZ;
    vertices3[9] = -1;//min_x;
    vertices3[10] = -1;//min_y;
    vertices3[11] = posZ;

}

Wall::Wall(float w, float h) :
    Wall()
{
    setRectangle(w, h);
}

Wall::~Wall()
{
    
}

void Wall::setColor(float r, float g, float b, float a)
{
    colors4Count = 4;
    if (colors4 == NULL)
        colors4 = new float[colors4Count * 4];
    int j = 0;
    for(int i = 0; i < colors4Count; i++)
    {
        colors4[j] = r / 255.0;
        colors4[j + 1] = g / 255.0;
        colors4[j + 2] = b / 255.0;
        colors4[j + 3] = a;
        j += 4;
    }

}

void Wall::setRectangle(float w, float h)
{
    wallWidth = w;
    wallHeight = h;
    passed = false;
}

const float* Wall::getModelView()
{
    //scale 1x1 box by width and height
    modelView[0] = wallWidth / 2;
    modelView[1] = 0;
    modelView[2] = 0;
    modelView[3] = 0;
    modelView[4] = 0;
    modelView[5] = wallHeight / 2;
    modelView[6] = 0;
    modelView[7] = 0;
    modelView[8] = 0;
    modelView[9] = 0;
    modelView[10] = 1;
    modelView[11] = 0;
    modelView[12] = posX;//x
    modelView[13] = posY;//y
    modelView[14] = posZ;//z
    modelView[15] = 1;
        
    return (const float*)&modelView;
}
