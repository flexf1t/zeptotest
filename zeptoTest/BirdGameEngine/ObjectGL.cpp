//
//  ObjectGL.cpp
//  zeptoTest
//
//  Created by Ivan Lapshin on 25.02.16.
//  Copyright © 2016 Ivan Lapshin. All rights reserved.
//

#include "ObjectGL.h"

ObjectGL::ObjectGL()
{
    shaderName.clear();
    vertices3 = NULL;
    colors4 = NULL;
    vertices3Count = 0;
    colors4Count = 0;
    posX = 0;
    posY = 0;
    posZ = 0;
    vertex3BufferName = -1;
    color4BufferName = -1;
    drawType = GL_TRIANGLE_STRIP;
}

ObjectGL::~ObjectGL()
{
    shaderName.clear();
    if (vertices3)
        delete [] vertices3;
    if (colors4)
        delete [] colors4;
}

const float* ObjectGL::getModelView()
{
    modelView[0] = 1;
    modelView[1] = 0;
    modelView[2] = 0;
    modelView[3] = 0;
    modelView[4] = 0;
    modelView[5] = 1;
    modelView[6] = 0;
    modelView[7] = 0;
    modelView[8] = 0;
    modelView[9] = 0;
    modelView[10] = 1;
    modelView[11] = 0;
    modelView[12] = posX;//x
    modelView[13] = posY;//y
    modelView[14] = posZ;//z
    modelView[15] = 1;
        
    return (const float*)&modelView;
}