//
//  ObjectGL.h
//  zeptoTest
//
//  Created by Ivan Lapshin on 25.02.16.
//  Copyright © 2016 Ivan Lapshin. All rights reserved.
//

#ifndef ObjectGL_h
#define ObjectGL_h

#include <stdio.h>
#include <string>
#include <OpenGLES/ES2/gl.h>

class ObjectGL
{
public:
    ObjectGL();
    virtual ~ObjectGL();
    virtual const float* getModelView();
    GLuint vertex3BufferName;
    GLuint color4BufferName;
    GLuint drawType;
    std::string shaderName;
    
    float* vertices3;
    int vertices3Count;
    float* colors4;
    int colors4Count;
    float posX;
    float posY;
    float posZ;
protected:
    float modelView[16];
};

#endif /* ObjectGL_h */
