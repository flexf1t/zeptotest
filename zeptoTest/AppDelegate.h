//
//  AppDelegate.h
//  zeptoTest
//
//  Created by Ivan Lapshin on 09.02.16.
//  Copyright © 2016 Ivan Lapshin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

