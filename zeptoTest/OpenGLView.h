//
//  OpenGLView.h
//  zeptoTest
//
//  Created by Ivan Lapshin on 08.02.16.
//  Copyright © 2016 Ivan Lapshin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>

@interface OpenGLView : UIView {
    EAGLContext*    context;
    CAEAGLLayer*    eaglLayer;
    
    GLuint          colorRenderbuffer;
    GLuint          depthRenderbuffer;
    GLuint          sampleColorRenderbuffer;
    GLuint          sampleDepthRenderbuffer;
    GLuint          framebuffer;
    GLuint          sampleFramebuffer;
    
    GLint           framebufferWidth;
    GLint           framebufferHeight;
    
    void*           game;
}
- (void)setup;
- (void)setupLayer;
- (void)setupContext;
- (void)setupFramebuffer;
- (void)setupDisplayLink;
- (void)setupMultisampleFramebuffer;

- (void)render:(CADisplayLink*)displayLink;
- (void)handleTap;

@end

