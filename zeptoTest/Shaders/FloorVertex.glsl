attribute vec3 position;
attribute vec4 sourceColor;

varying vec4 destinationColor;

uniform mat4 projection;
uniform mat4 modelView;

void main(void) {
    destinationColor = sourceColor;
    gl_Position = projection * modelView * vec4(position, 1.0);
}