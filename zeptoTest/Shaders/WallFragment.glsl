varying lowp vec4 destinationColor;

uniform lowp float time;

uniform lowp vec2 resolution;

void main(void) {
    gl_FragColor = destinationColor;
}