varying lowp vec4 destinationColor;

uniform highp float time;

uniform highp vec2 resolution;

void main(void) {
    highp vec2 uv = gl_FragCoord.xy / resolution;
   
    uv.x = fract(uv.x - uv.y + time) * 6.0;
    highp vec3 color = vec3(destinationColor);
    if (fract(uv.x) < 0.5)
        gl_FragColor = destinationColor;
    else
        gl_FragColor = vec4(color * 1.5, 1.0);
}