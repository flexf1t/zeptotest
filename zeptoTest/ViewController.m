//
//  ViewController.m
//  zeptoTest
//
//  Created by Ivan Lapshin on 09.02.16.
//  Copyright © 2016 Ivan Lapshin. All rights reserved.
//

#import "ViewController.h"
#import "OpenGLView.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)handleTap:(id)sender {
    [(OpenGLView*)self.view handleTap];
}

@end
